
# Best Reads

Best Reads is an application that allows users to search for any book ever-published and create a personal book catalogue.


#### 🔗 Live URL: 
TBD


## Table of content
- [Description](#description)
- [Screenshot](#screenshot)
- [Technologies](#technologies) 
- [Features](#features)
- [Future Additions](#future-additions)
- [Acknowledgements](#acknowledgements)

## Description
Best Reads is a full-stack application developed using Spring boot, thymeleaf and Cassandra. The app utilizes the Open Library API for its book search and mimics the famous Good Reads website, hence the name - Best Reads.


## Screenshot

![App Screenshot](https://via.placeholder.com/468x300?text=App+Screenshot+Here)


## Tech Stack

**Client:** Thymeleaf & Bootstrap

**Server & DB:** Spring boot & Cassandra


## Features
The project has the following features:

- Google & Github login
- Book search  
- CSRF protection


## Future Additions 

- Add 1000+ extra books and authors to app database.


## Acknowledgements

 - [Java Brains](https://javabrains.io/)
 - [Open Library API](https://openlibrary.org/)
 
