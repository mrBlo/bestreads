package com.afrifa.book;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.afrifa.userbooks.UserBooks;
import com.afrifa.userbooks.UserBooksPrimaryKey;
import com.afrifa.userbooks.UserBooksRepository;

@Controller
@RequestMapping("/books")
public class BookController {

	@Autowired
	BookRepository bookRepository;
	
	@Autowired
	UserBooksRepository userBooksRepository;

	private final String COVER_IMAGE_ROOT = "http://covers.openlibrary.org/b/id/";

	@GetMapping(value = "/{bookId}")
	public String getBook(@PathVariable String bookId, Model model, @AuthenticationPrincipal OAuth2User principal) {
		
		Optional<Book> optionalBook = bookRepository.findById(bookId);
		if (optionalBook.isPresent()) {
			Book book = optionalBook.get();

			// handle cover ids
			String coverImageUrl = "/images/no_image.png";
			if (book.getCoverIds() != null && book.getCoverIds().size() > 0) {
				coverImageUrl = COVER_IMAGE_ROOT + book.getCoverIds().get(0) + "-L.jpg ";
			}
			// Placing Objects into model so it's available in book.html #Thymeleaf
			model.addAttribute("coverImage", coverImageUrl);
			model.addAttribute("book", book); 
			
			// If user is logged in, show an added component
			if(principal != null && principal.getName() != null) {
				
				// principal.getName seems to be the unique principal ID for both Github & Google
				String userId= principal.getName();
				model.addAttribute("userLoginId", userId); 
				
				//check if user has interacted with book before
				UserBooksPrimaryKey key= new UserBooksPrimaryKey();
				key.setBookId(bookId);
				key.setUserId(userId);
				Optional<UserBooks> userBooks = userBooksRepository.findById(key);
				if(userBooks.isPresent()) {
					model.addAttribute("userBooks", userBooks.get());
				}
				else {
					//send empty userBooks Obj to template 
					model.addAttribute("userBooks", new UserBooks());
				}
				
			}
			
			return "book"; // render book.html
		}

		return "book-not-found";
	}

}
