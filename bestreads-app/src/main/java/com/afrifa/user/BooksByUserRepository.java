package com.afrifa.user;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;

public interface BooksByUserRepository extends CassandraRepository<BooksByUser, String> {

	//return a Slice since you're using a cassandra db. Pagination in cassandra is tricky hence we using Slice.
	Slice<BooksByUser> findAllById(String id, Pageable pageable);
	
}
