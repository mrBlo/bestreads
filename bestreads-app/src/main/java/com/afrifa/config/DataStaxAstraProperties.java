package com.afrifa.config;

import java.io.File;

import org.springframework.boot.context.properties.ConfigurationProperties;

/*
 * This class adds the datastax.astra property in application.properties to the application's class path
 */
@ConfigurationProperties(prefix = "datastax.astra")
public class DataStaxAstraProperties {

	private File secureConnectBundle;

//	Getter and Setter
	public File getSecureConnectBundle() {
		return secureConnectBundle;
	}

	public void setSecureConnectBundle(File secureConnectBundle) {
		this.secureConnectBundle = secureConnectBundle;
	}
	
	
	
}
