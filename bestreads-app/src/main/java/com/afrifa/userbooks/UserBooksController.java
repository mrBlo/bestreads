package com.afrifa.userbooks;

import java.time.LocalDate;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.ModelAndView;

import com.afrifa.book.Book;
import com.afrifa.book.BookRepository;
import com.afrifa.user.BooksByUser;
import com.afrifa.user.BooksByUserRepository;

@Controller
public class UserBooksController {
	
	private final Logger LOGGER= LoggerFactory.getLogger(UserBooksController.class);
	
	@Autowired
	UserBooksRepository userBooksRepository;
	
	@Autowired 
    BooksByUserRepository booksByUserRepository;
	
	@Autowired 
	BookRepository bookRepository;
	
	 
	@PostMapping("/add-user-book")
	public ModelAndView addBookForUser(
			@RequestBody MultiValueMap<String, String> formData,
			@AuthenticationPrincipal OAuth2User principal) {
		
		// if User isn't logged in, leave method
		if(principal == null || principal.getName() == null) {
			return null;
		}
		
		 String bookId = formData.getFirst("bookId");
	        Optional<Book> optionalBook = bookRepository.findById(bookId);
	        if (!optionalBook.isPresent()) {
	            return new ModelAndView("redirect:/");
	        }
	        Book book = optionalBook.get();
		

		//Get and parse rating from form
		int rating = Integer.parseInt(formData.getFirst("rating"));
		
		//Sort out the primary key for the userBooks obj
		UserBooksPrimaryKey primaryKey= new UserBooksPrimaryKey();
		String userId = principal.getName();
		primaryKey.setUserId(userId);
		primaryKey.setBookId(bookId);
		
		
		//Now create userBooks obj from formData
		UserBooks userBook= new UserBooks();
		userBook.setUserBooksPrimaryKey(primaryKey);
		userBook.setStartDate(LocalDate.parse(formData.getFirst("startDate")));
		userBook.setEndDate(LocalDate.parse(formData.getFirst("endDate")));
		userBook.setRating(rating);
		userBook.setReadingStatus(formData.getFirst("readingStatus"));
	
		//Persist userBooks obj to the DB
		userBooksRepository.save(userBook);
		LOGGER.info("UserBook [ "+userBook+" ] saved");
		
		//Now create BooksByUser obj to be used for the books read by a user.
		BooksByUser booksByUser = new BooksByUser();
        booksByUser.setId(userId);
        booksByUser.setBookId(bookId);
        booksByUser.setBookName(book.getName());
        booksByUser.setCoverIds(book.getCoverIds());
        booksByUser.setAuthorNames(book.getAuthorNames());
        booksByUser.setReadingStatus(formData.getFirst("readingStatus"));
        booksByUser.setRating(rating);
        booksByUserRepository.save(booksByUser);
        LOGGER.info("Books By User [ "+booksByUser+" ] saved");
		
		//upon persisting, redirect to book page(basically, do a refresh)
		return new ModelAndView("redirect:/books/"+bookId);
	}

}
