package com.afrifa.userbooks;

import java.time.LocalDate;

import org.springframework.data.cassandra.core.mapping.CassandraType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;
import org.springframework.data.cassandra.core.mapping.CassandraType.Name;

import lombok.Data;

/*
 * This model represents a user's interaction with a specific book.
 * it shows the readingStatus, start and end date etc.
 */
@Data
@Table(value = "bookinfo_by_user_and_book")
public class UserBooks {

	@PrimaryKey
	private UserBooksPrimaryKey userBooksPrimaryKey;
	
	@Column("reading_status")
	@CassandraType(type = Name.TEXT)
	private String readingStatus;

	@Column("start_date")
	@CassandraType(type = Name.DATE)
	private LocalDate startDate;

	@Column("end_date")
	@CassandraType(type = Name.DATE)
	private LocalDate endDate;

	@Column("rating")
	@CassandraType(type = Name.INT)
	private int rating;


	
	
}
