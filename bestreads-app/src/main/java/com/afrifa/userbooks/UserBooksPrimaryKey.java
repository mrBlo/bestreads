package com.afrifa.userbooks;


import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import lombok.Data;



/*
 * This primary key class was made because you can't have 2 primary keys in the UserBooks model class.
 * Hence this class's reference will be called as a Primary Key variable in the UserBooks class 
 * so that the UserBooksRepository gets only one primary key 
 * ie. ...UserBooksRepository extends CassandraRepository<UserBooks, UserBooksPrimaryKey> 
 */

@PrimaryKeyClass //This tells Spring Data that this class isn't an entity but is a primary key for an entity
@Data
public class UserBooksPrimaryKey {

	@PrimaryKeyColumn(name = "user_id", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
	private String userId;

	@PrimaryKeyColumn(name = "book_id", ordinal = 1, type = PrimaryKeyType.PARTITIONED)
	private String bookId;
	
}

/*
NB: The @Id annotation doesn't apply in a @PrimaryKeyClass
*/
