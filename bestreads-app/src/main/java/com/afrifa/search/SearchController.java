package com.afrifa.search;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;

/*
 * This controller is gonna call the Open Library API's search endpoint with user provided search params
 */
@Controller
public class SearchController {
	
	//Create Web client to make calls to Open Library API
	private final WebClient webClient;

    private final String COVER_IMAGE_ROOT = "http://covers.openlibrary.org/b/id/";
	
	//Build webClient once in constructor and use for all your calls
	public SearchController(WebClient.Builder webClientBuilder) {
		
        this.webClient = webClientBuilder
        		  .exchangeStrategies(ExchangeStrategies.builder()
        		  .codecs(configurer -> configurer
                  .defaultCodecs()
                  .maxInMemorySize(16 * 1024 * 1024))
                  .build())
        		.baseUrl("http://openlibrary.org/search.json")
        		.build();
	}

	//expecting url like ...?q=harry+potter 
	@GetMapping("/search")
	public String getSearchResults(@RequestParam String query, Model model) {
		//build request to be sent, send and handle response
		Mono<SearchResult> resultsMono = this.webClient.get()
					  .uri("?q={query}", query)
					  .retrieve()
					  .bodyToMono(SearchResult.class);
		SearchResult result= resultsMono.block();
		
		//show only 10 books 
		List<SearchResultBook> books =result.getDocs()
		.stream()
		.limit(10)
		.map(bookResult -> {
			//sanitize book objects
			bookResult.setKey(bookResult.getKey().replace("/works/", ""));
			  String coverId = bookResult.getCover_i();
              if (StringUtils.hasText(coverId)) {
                  coverId = COVER_IMAGE_ROOT + coverId + "-M.jpg";
              } else {
                  coverId = "/images/no_image.png";
              }
              bookResult.setCover_i(coverId);
			return bookResult;
		})
		.collect(Collectors.toList());
		
		model.addAttribute("searchResults", books);
		return "search";
	}
}

/* NB: Since the search results exceed the default limit of items set by Springboot (ie. you will get this exception -> 
DataBufferLimitException: Exceeded limit on max bytes to buffer : 262144) 
So we gotta increase the default buffer limit by supercharging the webClient ie.adding ExchangeStrategies.
*/