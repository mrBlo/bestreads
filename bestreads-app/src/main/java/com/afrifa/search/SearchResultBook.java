package com.afrifa.search;

import java.util.List;

import lombok.Data;

/*
 * This class mirrors the book object in the search results from the Open Library API 
 */
@Data
public class SearchResultBook {
	
	/* NB: The variables should match those from the search api 
	 so you don't have to do any manual casting */
	private String key;
	private String title;
	private String cover_i;
	private List<String> author_name;
	private int first_publish_year ;

}
