package com.afrifa.search;

import java.util.List;

import lombok.Data;

/*
 * This class mirrors the output of the search results from the Open Library API. 
 * It represents the structure of the search result returned by the 
 * Open Library API. 
 */
@Data
public class SearchResult {
	
	/* NB: The variables should match those from the search api 
	 so you don't have to do any manual casting */
	private int numFound;
	private List<SearchResultBook> docs;

}
