package com.afrifa;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.cassandra.CqlSessionBuilderCustomizer;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import com.afrifa.author.Author;
//import com.afrifa.author.Author;
import com.afrifa.author.AuthorRepository;
import com.afrifa.book.Book;
import com.afrifa.book.BookRepository;
import com.afrifa.config.DataStaxAstraProperties;

@SpringBootApplication
@EnableConfigurationProperties(DataStaxAstraProperties.class) // directing root class to location of configuration property
public class BestreadsDataLoaderApplication {
	@Autowired
	AuthorRepository authorRepository;
	
	@Autowired
	BookRepository bookRepository;
	

	@Value("${datadump.location.author}")
	private String authorDumpLocation;

	@Value("${datadump.location.works}")
	private String worksDumpLocation;

	private static final Logger LOGGER = LoggerFactory.getLogger(BestreadsDataLoaderApplication.class);
	
	
	public static void main(String[] args) {
		SpringApplication.run(BestreadsDataLoaderApplication.class, args);
	}

	/*
	 * In this method, w e're getting each line in the file as a stream and navigating to the index
	 * of "{", which denotes an author object that will be parsed as a JSON object
	 */
	private void initAuthors() {
		Path path = Paths.get(authorDumpLocation);
		try (Stream<String> lines = Files.lines(path)) // using try-with-resource
		{
			lines.forEach(line -> {
				// Read and parse the line
				String jsonString = line.substring(line.indexOf("{"));
				try {
					JSONObject jsonObject = new JSONObject(jsonString);

					// Construct the Author object
					Author author = new Author();
					author.setName(jsonObject.optString("name"));
					author.setPersonalName(jsonObject.optString("personal_name"));
					author.setId(jsonObject.optString("key").replace("/authors/", ""));

					// Persist using repository
					LOGGER.info("Saving author "+author.getName()+" ... ");
					authorRepository.save(author);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			});

		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	private void initWorks() {
		Path path = Paths.get(worksDumpLocation);
		DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS"); // 'T' denotes that ignore T(ie.time) in the date pattern eg.2009-12-11T01:59:20.516526
		try (Stream<String> lines = Files.lines(path)) 
		{
			lines.forEach(line -> {
				
				// Read and parse the line
				String jsonString = line.substring(line.indexOf("{"));
				try {
					JSONObject jsonObject = new JSONObject(jsonString);

					
					// Construct the Book object
					Book book= new Book();
					
					book.setId(jsonObject.getString("key").replace("/works/", ""));
					
					book.setName(jsonObject.optString("title"));
					
					JSONObject descriptionObj = jsonObject.optJSONObject("description");
					if(descriptionObj!=null) {
						book.setDescription(descriptionObj.optString("value"));
					}
					
					JSONObject createdObj = jsonObject.optJSONObject("created");
					if(createdObj != null) {
						String dateString= createdObj.optString("value");
						book.setPublishedDate(LocalDate.parse(dateString,dateFormat));
					}
					
					JSONArray coversJSONArray = jsonObject.optJSONArray("covers");
					if(coversJSONArray != null) {
						List<String> coverIds= new ArrayList<>();
						for(int i=0; i < coversJSONArray.length(); i++) {
							coverIds.add(coversJSONArray.getString(i));
						}
						book.setCoverIds(coverIds);
					}
					
					JSONArray authorsJSONArray = jsonObject.optJSONArray("authors");
					if(authorsJSONArray != null) {
						List<String> authorIds = new ArrayList<>();
						for(int i=0; i< authorsJSONArray.length(); i++) {
							JSONObject authorJSONobj = authorsJSONArray.getJSONObject(i);
							String authorId = authorJSONobj.getJSONObject("author")
									.getString("key")
									.replace("/authors/", "");
							authorIds.add(authorId);
						}
						book.setAuthorIds(authorIds);	
						/*
						 * Use each author key to get its corresponding author name from author_by_id table 
						 */
						List<String> authorNames = authorIds.stream()
						.map(id -> authorRepository.findById(id)) //produces an optional Author object
						.map(optionalAuthor -> {
							if(!optionalAuthor.isPresent()) return "Unknown Author";
							return optionalAuthor.get().getName();
						})
						.collect(Collectors.toList());
						book.setAuthorNames(authorNames);
					}
					

					// Persist using repository
					LOGGER.info("Saving book "+book.getName()+" ... ");
					bookRepository.save(book);
					

				} catch (JSONException e) {
					e.printStackTrace();
				}
			});

		}
		catch (IOException e) {
			e.printStackTrace();
		}

	}

	/* Persisting init authors and works in PostConstruct */
	@PostConstruct
	public void start() {
		initAuthors();
		initWorks();
	}

	/*
	 * This is necessary to have the Spring Boot app use the Astra secure bundle to
	 * connect to the database
	 */
	@Bean
	public CqlSessionBuilderCustomizer sessionBuilderCustomizer(DataStaxAstraProperties astraProperties) {
		Path bundle = astraProperties.getSecureConnectBundle().toPath();
		return builder -> builder.withCloudSecureConnectBundle(bundle);
	}

}
